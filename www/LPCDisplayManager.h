/**
 * @file LPCDisplayManager.cpp
 * @Author Jon Forsyth
 * @date 7/25/14
 * @brief Classes and functions used for displaying LPC magnitude spectrum and spectral peaks.
 */

#ifndef __lpc_app__LPCDisplayManager__
#define __lpc_app__LPCDisplayManager__

#include <GLKit/GLKit.h>

class DoubleBuffer;

typedef struct {
    GLKVector3 positionCoords; /**< OpenGL x,y,z position */
    GLKVector4 sceneColor;     /**< OpenGL color */
} SceneVertex;


/**
 * Class to manage display of LPC magnitude spectrum
 */
class LPCDisplayManager {
public:

    /**
     * Constructor
     * @param[in] numDisplayBins size of array used to display LPC magnitude spectrum
     * @param[in] sampleRate audio sample rate
     */
    LPCDisplayManager(UInt32 numDisplayBins, Float32 sampleRate);

    /**
     * Destructor
     */
    ~LPCDisplayManager();

    /**
     * Get the normalized frequency (value in [0.0,1.0]) from frequency in Hz (value in [0,sampleRate/2])
     * @param[in] freq frequency in Hz
     * @return normalized frequency 
     */
    Float32 getNormalizedFreq(Float32 freq);

    /**
     * Perform OpenGL initialization
     */
    void initOpenGL();
    
    /**
     * Render LPC magnitude spectrum to OpenGL using member OpenGL SceneVertex arrays
     * @param[in] lpc_mag_buffer LPC magnitude buffer to draw
     */
    void render(Float32 *lpc_mag_buffer);

    /**
     * Render LPC magnitude spectrum to OpenGL using externally declared OpenGL SceneVertex arrays
     * @param[in] lpc_mag_buffer LPC magnitude buffer to draw
     * @param[out] freqVertices SceneVertex containing points defining LPC magnitude spectrum
     * @param[out] peakVertices SceneVertex containing points defining lines indicating peaks in LPC magnitude spectrum
     */
    void render(Float32 *lpc_mag_buffer, SceneVertex *freqVertices, SceneVertex *peakVertices);

    /**
     * Render target formant frequency lines to OpenGL using externally declared OpenGL SceneVertex arrays
     * @param[out] targFreqVertices SceneVertex containing points defining lines indicating target formant frequencies in LPC magnitude spectrum
     * @param[in] targFormantFreqs array containing frequencies in Hz indicating target formant frequencies
     * @param[in] maxNumTargFormantFreqs number of target formant frequencies
     */
    void renderTargetFormantFreqs(SceneVertex *targFreqVertices, double *targFormantFreqs, int maxNumTargFormantFreqs);
    
    // properties
    Float32 m_sampleRate;           /**< audio sample rate */

    SceneVertex *_freqVertices;     /**< OpenGL vertices for drawing LPC magnitude spectrum (not currently used) */
    SceneVertex *_peakVertices;     /**< OpenGL vertices for drawing peaks in LPC magnitude spectrum (not currently used) */
    
    UInt32 m_numPeaks;              /**< number of peaks in LPC magnitude spectrum */
    
    UInt32 _numDisplayBins;         /**< size of array used to display LPC magnitude spectrum  */
    GLuint _vertexBufferID;         /**< OpenGL vertex buffer ID (not currently used) */
    
private:
    
    // properties
    DoubleBuffer *_historyBuffer;
    float _displayPad;
};


#endif /* defined(__lpc_app__GUIManager__) */
