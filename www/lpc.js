function callObjcWithURI(uri) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", uri);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
}

function setUsernames(jsonUsernames, selectedName) {
    usernames = JSON.parse(jsonUsernames);
    $("#accounts-group").empty();
    $.each(usernames, function(index, value) {
           elt = document.createElement("option");
           elt.text = value;
           if (value == selectedName)
                elt.selected = 'selected';
           $("#accounts-group").append(elt);
    });
    if (usernames.length == 0) {
        elt = document.createElement("option");
        elt.text = "PSEUDONYM";
        elt.selected = 'selected';
        elt.disabled = 'disabled';
        $("#accounts-group").append(elt);
    }
}

function createNewUsername() {
    callObjcWithURI("lpc://createuser");
    $("#accounts-group:disabled").selected = 'selected';
}

function deleteAllUsers() {
    callObjcWithURI("lpc://deleteallusers");
    $("#accounts-group:disabled").selected = 'selected';
}

function setCurrentUsername(username) {
    callObjcWithURI("lpc://selectuser/" + username);
}

function initializeUsers() {
    callObjcWithURI("lpc://initializeusers");
}

function styleRecordingButtonForState(isRecording) {
    if (isRecording) {
        $("#record").removeClass("recording").addClass("recording");
        $("#record").text("Stop");
    } else {
        $("#record").removeClass("recording");
        $("#record").text("Record");
    }
}

function toggleLPC() {
    callObjcWithURI("lpc://toggle");
}

function record() {
    callObjcWithURI("lpc://record");
}

function setLPCSize(x, y, width, height) {
    
    // Placeholder
    // Will at least prevent most error messages in Safari, Chrome ignores schemes it does
    // not recognize. Since much of the web interface is done in a browser,
    // would make sense to have an image placeholder to design around.
    // I have found Safari to better approximate the layout than Chrome.
    
    if(!/iPad|iPhone|iPod/.test(navigator.platform)) {
        //alert("This only works on iOS devices");
        return;
    }
    
	var query = 'lpc://setsize/x/' + x + '/y/' + y + '/width/' + width + '/height/' + height;
    callObjcWithURI(query);
}

function setFilterOrder(filterOrder) {
    callObjcWithURI("lpc://setfilterorder/" + filterOrder);
}
