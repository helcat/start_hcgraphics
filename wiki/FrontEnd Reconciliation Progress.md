---

### Directory: _css

00_base.css  
[x] merged   [ ] reconciled

01_LeftNav.css  
[] merged   [ ] reconciled

02_LPC.css
[] merged   [ ] reconciled

---
## Global JS Libs ##

### jQuery-ui.min   
http://learn.jquery.com/jquery-ui/getting-started/  
PURPOSE: LPC slider,  
PRODUCTION: build custom jQ-ui lib w/ <<purpose fx only>>  
DEPS: jQuery  

### touchpunch.min  
http://touchpunch.furf.com/  
PURPOSE: adds touch for jQ-ui elements (currently slider only)  
PRODUCTION: ok  
DEPS: jQ, jq-ui  

### lpc.js
lpc interface w/ ios code

### p5.js
we may not need this

### Dependency Only Libs
jQuery min

---

##Pages (App Hierarchy)

###00loadscreen
- This dir is currently empty. 
It is an a/v heavy loading animation, we’ll added back in when its needed.

01profiles

02tutorial

03auto

04free

05syllables

06words

07resources