#####Wiki Nav   
* [front-end reconciliation] (https://bitbucket.org/helcat/start_hcgraphics/wiki/Front%20End%20Reconciliation%20Progress)
* [hc's task list](https://bitbucket.org/helcat/start_hcgraphics/wiki/Home)  
* [Front-End Docs](https://bitbucket.org/helcat/start_hcgraphics/wiki/Front-End%20Docs)  

---
# hc Mtg Notes/Task List
src: [SPRING 2016 RELEASE SCHEDULE for staRT](https://workflowy.com/s/M5KVSRhWJ0)
---

## 1/30-2/5 - .6
**mtg: 2016-01-29**  

[ ] ADMIN: write to eric re: mgt space for Fris at 1p
- alt EdTech room is #821

[ ] ALL: Merge most updated interface code with version in repo  
[ ] ALL: remove font dependencies  
[ ] ALL:   
[ ] NAV: Did Nav really change?  

[ ] WORDS: Mock & Layout
- display a word, LPC w/ rec opts, btns for SLP scoring  
[ ] WORDS: UI controls for Rec & Stop on LPC  
[ ] WORDS: clinician scoring options (correct | incorrect)  

[ ] WORDS: Design of settings page  
- settings is page1 of Words  
- controls for age, sex, height, name, filter order  
--- talk to @Tim about 'lookup fx' ???  

[ ] TUT: Adjust spacing in tutorial (make room for LPC)  
[ ] TUT: Insert screenshots where there are currently placeholders  

[ ] NAV: fix inline SVG issues  

--------------------------------
### 2/5-2/12 - .7 ###  
**mtg 2016-02-05**   

Helen:   
Design of word practice page  
Present a word above the LPC  
Display correct/incorrect/intermediate buttons below LPC  
Display "end session" button off to side, continuously available  

Tim:  
Link buttons (settings, free play,word practice) to associated pages  
Look into options for remote install

Sam:   
Word practice routine  
Store table of /r/ words in app memory  
When user accesses "Word Practice" (button from side toolbar):  
Randomly select a word  
Display the text above the LPC   
Display three buttons below the LPC  
Repeat above routine until N words presented OR user presses "end session" button; then discontinue.  

### 2/12-2/19 - .8 ###  
Helen: Design intro and outro to word practice  
Intro: Display button "Click here to audio record your session."  
Subtext: "Later we will ask if you would like to upload your audio recording for use in our research. You are free to say no."
Outro: 
Tim: Enable remote install  


### 2/19-2/26 - .9 ###  
Debug all  

### 2/20 - 1.0 (version 1.0 release) ###  
Clean up source code listing  