# README #

This is a temporary repo for integrating graphics code from Spr15 into the Start code base. 

##Contents: 
###dir: start_hcGraphics 

- **ui_hc:** my graphics folder from spr15   
--- i'll design and test in this folder  

- **www:** clone of Tim's Start App's www folder   
---I'll implement code here and push to Tim's master

---

## [HC's To Do List](https://bitbucket.org/helcat/start_hcgraphics/wiki/Home)

---

**Set Up:** n/a

**Contributing:** n/a

**Owner:** hc 
(just IM me or leave me a note here, if needed)