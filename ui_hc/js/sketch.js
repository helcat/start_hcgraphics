/////// GLOBALS /////// 

var time = 5;


//////// CANVASSES ////////
var lpc = function(sketch) {
	var white = sketch.color(255,255,255); //#ffffff
	var sky = sketch.color(197,243,255);  //#C5F3FF;
	var ocean = sketch.color(83,200,233);  //#53C8E9;

    sketch.setup = function() {
    	var p5wave = sketch.createCanvas(740, 240);

      //append the canvas to existing div "wave" in DOM
      .parent("wave");

      sketch.frameRate(35);

  	}; //END setup

	sketch.draw = function() {
    sketch.background(sky);
    sketch.stroke(ocean);
    sketch.strokeWeight(4);

   var x = 0;

   while(x < sketch.width) {
    //ex.  sketch.line(x1,y1,x2,y2);
    sketch.line(x, 50+75 * sketch.noise(x/180, time), x, sketch.height);
    x += 1;
   }
   time += 0.01;

	}; //END draw
} // END lpc

/////// DISPLAY /////// 
// Create the p5 instance by passing your new canvas fx and specifing the div id
/*
window.onload = function() {
  var wave = document.getElementById( "wave" );
  var p5lpc = new p5(lpc, "wave");
};
*/